function solution() {
  const defaultResult = 0;

  let rootElements;

  const ulRootElements = Array.from(document.getElementsByTagName('ul')).filter(scopeToDirectDescendantsOfBody);
  const olRootElements = Array.from(document.getElementsByTagName('ol')).filter(scopeToDirectDescendantsOfBody);

  const defaultRootElements = [];
  if ((ulRootElements && ulRootElements.length > 0)) {
    rootElements = defaultRootElements.concat(ulRootElements);
  } else if ((olRootElements && olRootElements.length > 0)) {
    rootElements = defaultRootElements.concat(olRootElements);
  } else {
    return defaultResult;
  }

  let rootElement = rootElements[0];
  if (rootElement && rootElement.children) {
    return loopChildren(rootElement.children);
  }

  return defaultResult;
}

// Allows us to loop throught the children and if any, the childrens children.
function loopChildren(children) {
  let level = 0;

  for (let i = 0; i < children.length; i++) {
    const child = children[i];
    
    if (child.tagName.toLowerCase() === 'ul' || child.tagName.toLowerCase() === 'ol') {
      level++;
    }

    if(child.children) {
      level = level + loopChildren(child.children);
    }
  }

  return level;
}

function scopeToDirectDescendantsOfBody(elem) {
  return elem.parentNode && elem.parentNode.tagName.toLowerCase() === 'body';
}
