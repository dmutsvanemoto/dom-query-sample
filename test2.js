

function solution(rootElement) {
  if (rootElement && rootElement.children) {
    return loopChildren(rootElement.children);
  }

  return 0;
}

function loopChildren(children) {
  let level = 0;

  for (let i = 0; i < children.length; i++) {
    const child = children[i];
    
    if (child.tagName.toLowerCase() === 'ul' || child.tagName.toLowerCase() === 'ol') {
      level++;
    }

    if(child.children) {
      level = level + loopChildren(child.children);
    }
  }

  return level;
}


function processChildrenOfBody(tagName) {
  console.log(`Now processing ${tagName} elements`);

  // we only care about direct descendants of the body element.
  var bodyChildren = Array.from(document.getElementsByTagName(tagName))
  .filter(elem => elem.parentNode && elem.parentNode.tagName.toLowerCase() === 'body')

  for (let i = 0; i < bodyChildren.length; i++) {
    const child = bodyChildren[i];
    
    console.log(`('body' '${tagName}') element has ${solution(child)} nested level(s)`);
  }
}

// UL children
processChildrenOfBody('ul');

// OL children
processChildrenOfBody('ol');