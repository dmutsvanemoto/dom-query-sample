## Test 1
Create a function that returns `true` or `false` and accents two params;

Given an array (x) containing non-descending integers and an integer (k):
- Verify that the array contains every number from 1 to k at least once.
- Verify the array only contains numbers matching the above condition,


## Test 2

Create a function that returns a number represented the number of nested `ul`/`ol` elements.
Given some DOM containing elements `ul` and/or `ol` in any particular order.
- Return the count of nested `ul`/`ol` elements

Given the below sample the result should be 1
```html
<ul></ul>
```
Given the below sample the result should be 1
```html
<ul><ol></ol></ul>
```