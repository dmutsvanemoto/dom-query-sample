function solution(A, K) {
    var n = A.length;
    for (var i = 0; i < n - 1; i++) {
        if (A[i] + 1 < A[i + 1]) // perhaps less or equals
            return false;
    }
    if (A[0] != 1 || A[n - 1] != K) // || works better cause we want 
        return false;
    else
        return true;
}

solution([2,2,3,4,5,6,7,8,9,10], 10)
solution([1,2,3,4,5,6,7,8,9,10], 10); // returns true
solution([1,2,3,4,5,6,7,8,9,11], 10); // returns false
solution([1,2,3,4,5,6,7,8,9,10], 11); // return true when we want false
solution([1,2,3,4,5,6,7,8,9,11], 11); // returns false as expected
solution([1,2,3,4,5,6,7,8,9,10], 9) // return false
solution([1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10], 10); // returns true as expected